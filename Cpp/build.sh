#!/bin/bash
g++ hello.cpp -o cpptest
STATUS=$?
if [ $STATUS -eq 0 ]; then
echo "Deployment Successful"
./cpptest
ls
else
echo "Deployment Failed"
cd /opt/atlassian/pipelines/agent/build
pwd
git revert --no-commit HEAD
git commit -m "reverted build failed"
git push
exit 1
fi
