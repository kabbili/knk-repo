#!/bin/bash
mvn clean package
STATUS=$?
if [ $STATUS -eq 0 ]; then
   docker version
   #mvn org.sonarsource.scanner.maven:sonar-maven-plugin:sonar        
echo "Deployment Successful"
ls target
apt-get update && apt-get install -yq libgconf-2-4
apt-get update && apt-get install -y wget --no-install-recommends && \
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -  && \
sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' && \
apt-get update && \
apt-get install -y google-chrome-unstable fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst ttf-freefont --no-install-recommends
google-chrome --version
else
echo "Deployment Failed"
exit 1
fi
