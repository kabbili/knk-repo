#! /bin/bash
folder=`git diff --name-only --diff-filter=ADMR @~..@ | awk 'BEGIN {FS="/"} {print $1}' | uniq`
echo $folder
if [ "$folder" = "CSharp" ]
then
cd CSharp/TestCsharp/TestApp
ls
else
cd "$folder"
ls
fi
pwd
chmod 777 build.sh
./build.sh