#!/bin/bash
apt-get update -y
apt-get install -y mono-devel
mcs Program.cs
STATUS=$?
if [ $STATUS -eq 0 ]; then
echo "Deployment Successful"
ls
else
echo "Deployment Failed"
cd /opt/atlassian/pipelines/agent/build
pwd
git revert --no-commit HEAD
git commit -m "reverted build failed"
git push
exit 1
fi
