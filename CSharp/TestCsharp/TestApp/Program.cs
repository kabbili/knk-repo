﻿﻿﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee emp1 = new Employee();
            emp1.name = "Prasad";
            emp1.address = "Sodam";
            emp1.Display();
            Employee emp2 = new Employee();
            emp2.name = "Harshith";
            emp2.address = "Sodam";
            emp2.Display();
            Employee kiran = new Employee();
            kiran.name = "Kiran";
            kiran.address = "Sodam";
            kiran.Display();
            Console.ReadLine();
        }
    }
}

